import React, { Component } from 'react';
import './App.css';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import About from "./containers/About";
import Admin from "./containers/Admin";

class App extends Component {
    render() {
        return (
            <div className ='App'>
            <Layout >
                <Switch>
                    <Route path="/pages/:id" exact component={About} />
                    <Route path="/admin" exact component={Admin} />
                    <Route render={() => <h1>404 Not found</h1>} />
                </Switch>
            </Layout>
            </div>
    );
    }
}

export default App;
