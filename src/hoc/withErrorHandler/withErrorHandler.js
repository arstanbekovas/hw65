import React, {Component, Fragment} from 'react';
import Modal from "../../components/Modal/Modal";

const withErrorHandler = (WrappedComponent, axios) => {
    return class extends Component {
        constructor(props) {
            super(props);

            this.state = {
                error: null
            };

           this.state.interceptorsId = axios.interceptors.response.use(res => res, (error) => {
                this.setState({error});
            })
        }

        componentWillUnmount() {
            axios.interceptors.response.eject(this.state.interceptorsId)
        }

        errorDismissed = () => {
            this.setState({error: null});
        };


        render() {
            return (
                <Fragment>
                    <Modal show={this.state.error} closed={this.errorDismissed}>This is error message</Modal>
                    <WrappedComponent {...this.props} />
                </Fragment>
            );
        }
    }
};

export default withErrorHandler;
