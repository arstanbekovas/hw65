import React from 'react';
import {NavLink} from "react-router-dom";
import './NavigationItems.css';

const NavigationItems = () => (
    <ul className="NavigationItems">
        <li className="NavigationItem">
            <NavLink to="/pages/about" exact>About</NavLink>
        </li>

        <li className="NavigationItem">
            <NavLink to={"/pages/contacts"} exact>Contacts</NavLink>
        </li>
        <li className="NavigationItem">
            <NavLink to={"/pages/divisions"} exact>Divisions</NavLink>
        </li>
        <li className="NavigationItem">
        <NavLink to={"/admin"} exact>Admin</NavLink>
    </li>
    </ul>
);

export default NavigationItems;

