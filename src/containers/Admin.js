import React, {Component} from 'react';
import axios from '../axios-orders';
import './Admin/Admin.css'

class Admin extends Component {
    state = {
        page:  {
            title: '',
            content: ''
        },
        loading: false,
        currentPage: ''
    };

    componentDidMount(){
        axios.get(`/${this.state.currentPage}.json`).then((response) =>{
            console.log(response.data);
            this.setState({loadedPost: response.data, page: response.data})
        })
    }
    savePostHandler = e => {
        e.preventDefault();

        this.setState({loading: true});
        const currentPage = this.state.currentPage;
        axios.patch(`/${currentPage}.json`, this.state.page).then(response => {
            this.setState({loading: false});
            this.props.history.replace('pages/about/');
        });
    };

    postValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                page: {...prevState.post, [e.target.name]: e.target.value}
            }
        });
    };

    changePage = (e) => {
        const currentPage = e.target.value;
        axios.get(`/${currentPage}.json`).then((response) =>{
            console.log(response.data);
            this.setState({loadedPost: response.data, page: response.data, currentPage})
        })
    };


    render(){
        return(
            <form className="EditPage">
                <h1>Edit  Pages</h1>

                <select className='EditPage' onChange={this.changePage}>
                    <option value='about'>About</option>
                    <option value='contacts'>Contacts</option>
                    <option value='divisions'>Divisions</option>
                    <option value='admin'>Admin</option>
                </select>
                <input className='EditPage' type="text" name="title" placeholder="Title" value={this.state.page.title} onChange={this.postValueChanged}/>
                <textarea className='EditPage' name="content" placeholder="Description" value={this.state.page.content} onChange={this.postValueChanged}>
                </textarea>

                <button className='EditPage' onClick={this.savePostHandler}>Save</button>
            </form>
        )
    }
}
export default Admin;