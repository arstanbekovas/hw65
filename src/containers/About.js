import React, {Component, Fragment} from 'react';
import axios from '../axios-orders'

class About extends Component {
    state = {
        page:  {
            title: '',
            content: ''
        }
};
    componentDidMount(){
        const id = this.props.match.params.id;

        axios.get(`/${id}.json`).then((response) =>{
            console.log(response.data);
            this.setState({page: response.data})
        })
    }
    componentDidUpdate(prevProps){
        const id = this.props.match.params.id;

        if(prevProps.match.params.id !== id) {
            axios.get(`/${id}.json`).then((response) =>{
                console.log(response.data);
                this.setState({page: response.data})
            })
        }

    }
    render () {
        return (
            <Fragment>
                <h1> {this.state.page.title}</h1>
                <p>{this.state.page.content}</p>
            </Fragment>

        )
    }

};

export default About;